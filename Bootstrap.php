<?php
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');

class Facebook_Bootstrap extends Zend_Application_Module_Bootstrap {
    /**
     * _initAutoload()
     *
     * Initialize the autoloader and return to bootstrap
     *
     * @return mixed
     */
    protected function _initAutoload() {
        // Add autoloader empty namespace
        $autoLoader = Zend_Loader_Autoloader::getInstance();
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
           'basePath'=>APPLICATION_PATH."/modules/facebook",
            'namespace'=>'',
            'resourceTypes'=>array(
              'form'=>array(
                  'path'=>'forms/',
                  'namespace'=>'Form_',
              ),
            ),
        ));
        // Return it so that it can be stored by the bootstrap
        return $autoLoader;
    }
}