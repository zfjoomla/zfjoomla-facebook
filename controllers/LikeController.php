<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * LikeController
 *
 * Facebook like module
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 */
class Facebook_LikeController extends Core_Controller_Action
{
    /**
     * @var string the title for the page
     */
    protected $_title="Facebook Like";
    /**
     * @var string the name of the zend module type
     */
    protected $_moduleName = 'mod_zend';
    /**
     * @var int the width of the display 
     */
    public $width = 450;
    /**
     * @var int number of posts to display
     */
    public $numPosts = 5;
    /**
     * @var string Facebook app id
     */
    protected $_appId = NULL;
    /**
     * @var string Facebook admin Id
     */
    protected $_adminId = NULL;
    /**
     * initializes the class
     */
    public function init()
    {
        parent::init();
        if(isset($this->_addParams) && isset($this->_addParams->appId)) {
            $this->_appId = $this->_addParams->appId;
        }
        if(isset($this->_addParams) && isset($this->_addParams->adminId)) {
            $this->_adminId = $this->_addParams->adminId;
        }
        if(isset($this->_addParams) && isset($this->_addParams->width)) {
            $this->width = $this->_addParams->width;
        }
        if(isset($this->_addParams) && isset($this->_addParams->sendButton)) {
            $this->sendButton = $this->_addParams->sendButton;
        }
        if(isset($this->_addParams) && isset($this->_addParams->fbLayout)) {
            $this->fbLayout = $this->_addParams->fbLayout;
        }
        if(isset($this->_addParams) && isset($this->_addParams->showFaces)) {
            $this->showFaces = $this->_addParams->showFaces;
        }
        if(isset($this->_addParams) && isset($this->_addParams->colorScheme)) {
            $this->colorScheme = $this->_addParams->colorScheme;
        }
    }
    /**
     * indexAction
     *
     * Shows Hello World
     */
    public function indexAction()
    {
        $document = &JFactory::getDocument();
        if (isset($this->_adminId)) {
            $document->addCustomTag('<meta property="fb:admins" content="' . $this->_adminId . '" />');
        } else if(isset($this->_appId)) {
            $document->addCustomTag('<meta property="fb:app_id" content="' . $this->_appId . '" />');
        } 
        // ensure we are using the correct locale
        $locale = new Zend_Locale();
        $this->view->locale = $locale->toString();
        // get the relevant arguments
        $this->view->fbUrl = (isset($_SERVER['HTTPS']))?'https://':'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $this->view->width = $this->width;
        $this->view->sendButton = $this->sendButton;
        $this->view->fbLayout = $this->fbLayout;
        $this->view->showFaces = $this->showFaces;
        $this->view->colorScheme = $this->colorScheme;
        if(isset($this->_appId)) {
            $this->view->appId = $this->_appId;
        }
    }
    /**
     * adminindexAction
     *
     * show the company administration screen
     */
    public function adminindexAction()
    {
        try {
            if($this->_application->isAdmin()) {
                $this->createMenu();
                // create the models
                $mdlModules = new Model_Modules();
                $this->view->modules = $mdlModules->getModules($this->_title);
            }
        } catch(exception $ex) {
            echo $ex->__toString();
        }
    }
    /**
     * createMenu
     *
     * builds the admin menu options for this controller
     */
    public function createMenu()
    {
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_($this->_title));
                // create the models
                $mdlModules = new Model_Modules();
                // we should have a valid install of zend_module by now so lets see if there is a module
                $modules = $mdlModules->getModuleByTitle($this->_title);
                if($modules==NULL) {
                    // a module does not exist so create one
                    $mdlModules->addModule(
                            $this->_title, // title
                            '', // note
                            '', // content
                            'user1', // position
                            $this->_moduleName, //module
                            1, // access
                            1, // showtitle 
                            array(
                                'zmodule'=>'Facebook',
                                'zcontroller'=>'like',
                                'zindex'=>'index',
                                'addparams'=>  json_encode(array(
                                    'title'=>$this->_title,
                                    'sendButton'=>true,
                                    'showFaces'=>true,
                                    'width'=>'500',
                                    'fbLayout'=>'standard',
                                )),
                            ), // params 
                            0 // client_id
                        );
                }
            }
        } catch(exception $ex) {
            echo "Error creating menu:".$ex->__toString();
        }
    }
    /**
     * addAction
     * 
     * shows and processes the add form
     */
    public function addAction()
    {
        // create the form
        $frmLike = new Facebook_Form_Like();
        // create the model
        $mdlModule = new Model_Modules();
        // check the like form
        if($this->buildForm($frmLike)) {
            $addParams = array(
                'title'=>$frmLike->getValue('title'),
                'width'=>$frmLike->getValue('width'),
                'fbLayout'=>$frmLike->getValue('fbLayout'),
                'sendButton'=>$frmLike->getValue('sendButton'),
                'showFaces'=>$frmLike->getValue('showFaces'),
                'colorScheme'=>$frmLike->getValue('colorScheme'),
            );
            // gather the ids
            $appId = $frmLike->getValue('appId');
            $adminId = $frmLike->getValue('adminId');
            if(strlen($appId)>0) {
                $addParams['appId'] = $appId;
            }
            if(strlen($adminId)>0) {
                $addParams['adminId']=$adminId;
            }
            
            $mdlModule->addModule(
                     $this->_title, // title
                            '', // note
                            '', // content
                            'user1', // position
                            $this->_moduleName, //module
                            1, // access
                            1, // showtitle 
                            array(
                                'zmodule'=>'Facebook',
                                'zcontroller'=>'like',
                                'zindex'=>'index',
                                'addparams'=>  json_encode($addParams),
                            ), // params 
                            0 // client_id
                    );
            // success
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false)."&message=".urlencode($this->view->translate->_("UPDATESUCCESS")));
        } 
        $this->view->form = $frmLike;
    }
    /**
     * editAction
     * 
     * shows and process the edit form
     */
    public function editAction()
    {
        // get the module id
        if(isset($this->_addParams) && isset($this->_addParams->id)) {
            $moduleId = $this->_addParams->id;
        } else {
            $this->_redirect($this->view->Jurl(array(
                            'module'=>$this->_module,
                            'controller'=>$this->_name,
                            'action'=>'adminindex',
                        ),'administrator',false)."&error=".urlencode($this->view->translate->_("INVALIDMODULEID")));
        }
        // create the form
        $frmLike = new Facebook_Form_Like();
        // create the model
        $mdlModule = new Model_Modules();
        // check the like form
        if($this->buildForm($frmLike)) {
            $addParams = array(
                'title'=>$frmLike->getValue('title'),
                'width'=>$frmLike->getValue('width'),
                'fbLayout'=>$frmLike->getValue('fbLayout'),
                'sendButton'=>$frmLike->getValue('sendButton'),
                'showFaces'=>$frmLike->getValue('showFaces'),
                'colorScheme'=>$frmLike->getValue('colorScheme'),
            );
            // gather the ids
            $appId = $frmLike->getValue('appId');
            $adminId = $frmLike->getValue('adminId');
            if(strlen($appId)>0) {
                $addParams['appId'] = $appId;
            }
            if(strlen($adminId)>0) {
                $addParams['adminId']=$adminId;
            }
            // get the module 
            $module = $mdlModule->getModule($moduleId);
            if(strlen($module->params)>0) {
                $formValues = json_decode($module->params);
                $formValues->addparams = json_encode($addParams);
                $mdlModule->updateModule($moduleId, array('params'=>  $formValues));
            }
            
            // success
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false)."&message=".urlencode($this->view->translate->_("UPDATESUCCESS")));
        } else {
            // prepopulate the form
            $module = $mdlModule->getModule($moduleId);
            if($module!= NULL) {
                if(strlen($module->params)>0) {
                    $formValues = json_decode($module->params);
                    if(isset($formValues->addparams)) {
                        $addparams = json_decode($formValues->addparams);
                        foreach($addparams as $name=>$value) {
                            //echo $name.";".$value."<br />";
                            switch($name) {
                                case 'sendButton':
                                case 'fbLayout':
                                case 'showFaces':
                                case 'appId':
                                case 'adminId':
                                case 'width':
                                case 'colorScheme':
                                case 'title':
                                    $frmLike->getElement($name)->setValue($value);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                $frmLike->getElement('id')->setValue($moduleId);
            }
        }
        $this->view->form = $frmLike;
    }
    /**
     * buildForm
     *
     * builds the like form
     *
     * @param Zend_Form $frmLike
     * @return boolean
     */
    public function buildForm(&$frmLike)
    {
        if(isset($this->_addParams) && isset($this->_addParams->id)) {
            $moduleId = $this->_addParams->id;
        }
        if($this->_getParam('Cancel')!=NULL||$this->_getParam('Cancel_x')!=NULL) {
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false));
        }        
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_($this->_title. " Module"));
                if($this->getRequest()->isPost()) {
                    if($frmLike->isValid($_POST)) {
                        return true;
                    }
                }
                
            }
        } catch (exception $ex) {
                echo $ex->__toString();
        }
        return false;
    }
}