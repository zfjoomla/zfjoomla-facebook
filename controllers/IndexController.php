<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * IndexController
 *
 * Default controller
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 */
class Facebook_IndexController extends Core_Controller_Action
{
    /**
     * @var string the name of the zend module
     */
    protected $_ZendModuleName = "Zend Module";
    /**
     * indexAction
     *
     * Shows Hello World
     */
    public function indexAction()
    {
        $this->view->hello = $this->view->translate->_("TITLE");
    }
    /**
     * adminindexAction
     * 
     * shows Hello World
     */
    public function adminindexAction()
    {
       try {
            if($this->_application->isAdmin()) {
                $this->createMenu();
                // create the necessary Models
                $mdlExtension = new Model_Components();
                $mdlModules = new Model_Modules();
                // check for a valid zend_module install
                $ZendModel = $mdlExtension->getModuleByName($this->_ZendModuleName);
                if($ZendModel==NULL) {
                    throw new Exception('The Zend Module is required in addition to ZFJoomla');
                }
                
            }
       } catch (Exception $ex) {
           echo $ex->__toString();
       }
    }
    /**
     * createMenu
     *
     * builds the admin menu options for this controller
     */
    public function createMenu()
    {
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_("TITLE"));
                // create the models
                $mdlModules = new Model_Modules();
                // main menu options
                $menuItems = array();
                $count = 0;
                // add menu Items
                // config menu item
                $menuItems[$count++] = array(
                    'link'=>JRoute::_($this->view->url(array(
                            'module'=>$this->_module,
                            'controller'=>'comments',
                            'action'=>'adminindex',
                        ),'administrator',true)),
                    'icon'=>'header'.DIRECTORY_SEPARATOR.'icon-48-article.png',
                    'text'=>'Comments',
                );
                $menuItems[$count++] = array(
                    'link'=>JRoute::_($this->view->url(array(
                            'module'=>$this->_module,
                            'controller'=>'like',
                            'action'=>'adminindex',
                        ),'administrator',true)),
                    'icon'=>'../../../components/'.ZEND_COMPONENT_NAME
                        .'/application/modules/Facebook/images/like.jpg',
                    'text'=>'Like',
                );
                
                $menuItems[$count++] = array(
                    'link'=>JRoute::_($this->view->url(array(
                            'module'=>$this->_module,
                            'controller'=>'send',
                            'action'=>'adminindex',
                        ),'administrator',true)),
                    'icon'=>'header'.DIRECTORY_SEPARATOR.'icon-48-inbox.png',
                    'text'=>'Send',
                );
                
                // send the menu items to the view
                $this->view->menuItems=$menuItems;
            }
        } catch(exception $ex) {
            echo "Error creating menu:".$ex->__toString();
        }
    }
}