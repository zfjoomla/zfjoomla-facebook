<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * CommentsController
 *
 * Facebok Comments controller
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 */
class Facebook_CommentsController extends Core_Controller_Action
{
    /**
     * @var string the title for the page
     */
    protected $_title="Facebook Commments";
    /**
     * @var string the name of the zend module type
     */
    protected $_moduleName = 'mod_zend';
    /**
     * @var int the width of the display 
     */
    public $width = 500;
    /**
     * @var int number of posts to display
     */
    public $numPosts = 5;
    /**
     * @var string Facebook app id
     */
    protected $_appId = NULL;
    /**
     * @var string Facebook admin Id
     */
    protected $_adminId = NULL;
    /**
     * initializes the class
     */
    public function init()
    {
        parent::init();
        if(isset($this->_addParams) && isset($this->_addParams->appId)) {
            $this->_appId = $this->_addParams->appId;
        }
        if(isset($this->_addParams) && isset($this->_addParams->adminId)) {
            $this->_adminId = $this->_addParams->adminId;
        }
        if(isset($this->_addParams) && isset($this->_addParams->width)) {
            $this->width = $this->_addParams->width;
        }
        if(isset($this->_addParams) && isset($this->_addParams->numPosts)) {
            $this->numPosts = $this->_addParams->numPosts;
        }
        if(isset($this->_addParams) && isset($this->_addParams->colorScheme)) {
            $this->colorScheme = $this->_addParams->colorScheme;
        }
    }
    /**
     * indexAction
     *
     * Shows Hello World
     */
    public function indexAction()
    {
        $document = &JFactory::getDocument();
        if (isset($this->_adminId)) {
            $document->addCustomTag('<meta property="fb:admins" content="' . $this->_adminId . '" />');
        } else if(isset($this->_appId)) {
            $document->addCustomTag('<meta property="fb:app_id" content="' . $this->_appId . '" />');
        } 
        // ensure we are using the correct locale
        $locale = new Zend_Locale();
        $this->view->locale = $locale->toString();
        // get the relevant arguments
        $this->view->fbUrl = (isset($_SERVER['HTTPS']))?'https://':'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $this->view->numPosts = $this->numPosts;
        $this->view->width = $this->width;
        $this->view->colorScheme = $this->colorScheme;
    }
    /**
     * adminindexAction
     *
     * show the company administration screen
     */
    public function adminindexAction()
    {
        try {
            if($this->_application->isAdmin()) {
                $this->createMenu();
                // create the models
                $mdlModules = new Model_Modules();
                $this->view->modules = $mdlModules->getModules($this->_title);
            }
        } catch(exception $ex) {
            echo $ex->__toString();
        }
    }
    /**
     * createMenu
     *
     * builds the admin menu options for this controller
     */
    public function createMenu()
    {
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_($this->_title));
                // create the models
                $mdlModules = new Model_Modules();
                // we should have a valid install of zend_module by now so lets see if there is a module
                $modules = $mdlModules->getModuleByTitle($this->_title);
                if($modules==NULL) {
                    // a module does not exist so create one
                    $mdlModules->addModule(
                            $this->_title, // title
                            '', // note
                            '', // content
                            'user1', // position
                            $this->_moduleName, //module
                            1, // access
                            1, // showtitle 
                            array(
                                'zmodule'=>'Facebook',
                                'zcontroller'=>'comments',
                                'zindex'=>'index',
                                'addparams'=>  json_encode(array(
                                    'title'=>$this->_title,
                                    'numPosts'=>5,
                                    'width'=>'500',
                                )),
                            ), // params 
                            0 // client_id
                        );
                }
            }
        } catch(exception $ex) {
            echo "Error creating menu:".$ex->__toString();
        }
    }
    /**
     * addAction
     * 
     * shows and processes the add form
     */
    public function addAction()
    {
        // create the form
        $frmComments = new Facebook_Form_Comments();
        // create the model
        $mdlModule = new Model_Modules();
        // check the comments form
        if($this->buildForm($frmComments)) {
            $addParams = array(
                'title'=>$frmComments->getValue('title'),
                'width'=>$frmComments->getValue('width'),
                'numPosts'=>$frmComments->getValue('numPosts'),
                'colorScheme'=>$frmComments->getValue('colorScheme'),
            );
            // gather the ids
            $appId = $frmComments->getValue('appId');
            $adminId = $frmComments->getValue('adminId');
            if(strlen($appId)>0) {
                $addParams['appId'] = $appId;
            }
            if(strlen($adminId)>0) {
                $addParams['adminId']=$adminId;
            }
            
            $mdlModule->addModule(
                     $this->_title, // title
                            '', // note
                            '', // content
                            'user1', // position
                            $this->_moduleName, //module
                            1, // access
                            1, // showtitle 
                            array(
                                'zmodule'=>'Facebook',
                                'zcontroller'=>'comments',
                                'zindex'=>'index',
                                'addparams'=>  json_encode($addParams),
                            ), // params 
                            0 // client_id
                    );
            // success
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false)."&message=".urlencode($this->view->translate->_("UPDATESUCCESS")));
        } 
        $this->view->form = $frmComments;
    }
    /**
     * editAction
     * 
     * shows and process the edit form
     */
    public function editAction()
    {
        // get the module id
        if(isset($this->_addParams) && isset($this->_addParams->id)) {
            $moduleId = $this->_addParams->id;
        } else {
            $this->_redirect($this->view->Jurl(array(
                            'module'=>$this->_module,
                            'controller'=>$this->_name,
                            'action'=>'adminindex',
                        ),'administrator',false)."&error=".urlencode($this->view->translate->_("INVALIDMODULEID")));
        }
        // create the form
        $frmComments = new Facebook_Form_Comments();
        // create the model
        $mdlModule = new Model_Modules();
        // check the comments form
        if($this->buildForm($frmComments)) {
            $addParams = array(
                'title'=>$frmComments->getValue('title'),
                'width'=>$frmComments->getValue('width'),
                'numPosts'=>$frmComments->getValue('numPosts'),
                'colorScheme'=>$frmComments->getValue('colorScheme'),
            );
            // gather the ids
            $appId = $frmComments->getValue('appId');
            $adminId = $frmComments->getValue('adminId');
            if(strlen($appId)>0) {
                $addParams['appId'] = $appId;
            }
            if(strlen($adminId)>0) {
                $addParams['adminId']=$adminId;
            }
            // get the module 
            $module = $mdlModule->getModule($moduleId);
            if(strlen($module->params)>0) {
                $formValues = json_decode($module->params);
                $formValues->addparams = json_encode($addParams);
                $mdlModule->updateModule($moduleId, array('params'=>  $formValues));
            }
            
            // success
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false)."&message=".urlencode($this->view->translate->_("UPDATESUCCESS")));
        } else {
            // prepopulate the form
            $module = $mdlModule->getModule($moduleId);
            if($module!= NULL) {
                if(strlen($module->params)>0) {
                    $formValues = json_decode($module->params);
                    if(isset($formValues->addparams)) {
                        $addparams = json_decode($formValues->addparams);
                        foreach($addparams as $name=>$value) {
                            //echo $name.";".$value."<br />";
                            switch($name) {
                                case 'numPosts':
                                case 'appId':
                                case 'adminId':
                                case 'width':
                                case 'title':
                                    $frmComments->getElement($name)->setValue($value);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                $frmComments->getElement('id')->setValue($moduleId);
            }
        }
        $this->view->form = $frmComments;
    }
    /**
     * buildForm
     *
     * builds the comments form
     *
     * @param Zend_Form $frmComments
     * @return boolean
     */
    public function buildForm(&$frmComments)
    {
        if(isset($this->_addParams) && isset($this->_addParams->id)) {
            $moduleId = $this->_addParams->id;
        }
        if($this->_getParam('Cancel')!=NULL||$this->_getParam('Cancel_x')!=NULL) {
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>$this->_name,
                    'action'=>'adminindex',
                ),'administrator',false));
        }        
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_($this->_title. " Module"));
                if($this->getRequest()->isPost()) {
                    if($frmComments->isValid($_POST)) {
                        return true;
                    }
                }
                
            }
        } catch (exception $ex) {
                echo $ex->__toString();
        }
        return false;
    }
}