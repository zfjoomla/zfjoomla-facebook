<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * Facebook comments
 *
 * the admin controlls for the facebook comments module
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Facebook
 */
class Facebook_Form_Comments extends Zend_Form
{
    /**
     * init
     *
     * Initializes the form
     * */
    public function init()
    {
        // set the default method
        $this->setMethod('post');
        $this->setName('Comments');

        // id
        $this->addElement($this->createElement('hidden','id',array()));
        
         // name
         $this->addElement($this->createElement('text','title',array(
                'label'=>'Title:',
                'required'=>true,
                'size'=>100,
         )));
         
         // appid
         $this->addElement($this->createElement('text','appId',array(
                'label'=>'Facebook Application Id:',
                'size'=>50,
         )));
         
         // adminId
         $this->addElement($this->createElement('text','adminId',array(
                'label'=>'Facebook Admin Id:',
                'size'=>50,
         )));
         
         // width
         $this->addElement($this->createElement('text','width',array(
                'label'=>'Display Width:',
                'required'=>true,
                'size'=>50,
         )));
         
         // numposts
         $this->addElement($this->createElement('text','numPosts',array(
                'label'=>'Number of posts to show:',
                'required'=>true,
                'size'=>50,
         )));
         
         $this->addElement($this->createElement('select', 'colorScheme',array(
             'Label'=>'Color Scheme:',
             'multioptions'=>array(
                 'light'=>'light',
                 'dark'=>'dark',
             ),
         )));


        //submit
         $this->addElement($this->createElement('submit','Save',array(
                'label'=>'Save',
                'class'=>'ui-button ui-state-default ui-corner-all',
         )));

         //cancel
         $this->addElement($this->createElement('submit','Cancel',array(
                'label'=>'Cancel',
                'class'=>'ui-button ui-state-default ui-corner-all',
         )));
    }

}

